import static driver.DriverManager.DRIVER;
import static configuration.Config.CONFIG;
import static driver.AppiumServer.APPIUM_SERVICE;
import static driver.BrowserstackLocalServer.BROWSERSTACK_SERVICE;
import org.testng.ITestContext;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;
import com.codeborne.selenide.WebDriverRunner;

public class BaseClass {
	
	public static String Search = "";
	
	 @BeforeClass(alwaysRun = true)
	 @Parameters(value = {"url", "platform"} )
	    public void beforeTest(ITestContext context, String url, String platform) {
		 
		 try {
		 	CONFIG.setTestEnvironment();
			
		 	switch (platform)
	        {
	        case "Chrome":
	        	DRIVER.chromeDriverConnection();
		        DRIVER.maximizeBrowser();
		        WebDriverRunner.setWebDriver(DRIVER.getURL(url));
	        	break;
	        case "Firefox":
	        	DRIVER.firefoxDriverConnection();
		        DRIVER.maximizeBrowser();
		        WebDriverRunner.setWebDriver(DRIVER.getURL(url));
	        	break;
	        case "Edge":
	        	DRIVER.edgeDriverConnection();
		        DRIVER.maximizeBrowser();
		        WebDriverRunner.setWebDriver(DRIVER.getURL(url));
	        	break;
	        case "android":
	        	DRIVER.setDriver();
	        	WebDriverRunner.setWebDriver(DRIVER.getDriver());
	        	break;
	        case "ios":
	        	DRIVER.setDriver2();
	        	WebDriverRunner.setWebDriver(DRIVER.getDriver());
	        	break;
	        case "browserstack":
	        	DRIVER.setDriver3();
	        	WebDriverRunner.setWebDriver(DRIVER.getDriver());
	        	break;
	        case "browserstack_android":
	        	DRIVER.setDriver4();
	        	WebDriverRunner.setWebDriver(DRIVER.getDriver());
	        	break;
	        case "browserstack_web":
	        	BROWSERSTACK_SERVICE.start();
	        	DRIVER.setDriver5();
	        	WebDriverRunner.setWebDriver(DRIVER.getDriver2());
	        	DRIVER.getDriver2().manage().window().maximize();
	        	DRIVER.getDriver2().get(url);
	        	break;
	        	
	        } 	
	        
		 	Search = System.getProperty("Search");
		 }
		 catch(Exception e)
		 {
			 System.out.println(e);
		 }
		 	
	 }

	 
	 @AfterClass(alwaysRun = true)
	    public void afterTest(final ITestContext testContext) {
		
	    try {
	    DRIVER.closeDriver();
	    APPIUM_SERVICE.stop();
	    BROWSERSTACK_SERVICE.stop();
	    }
	    catch(Exception e)
	    {
	    	//Do Something
	    }
	    }

}
