import org.testng.annotations.Test;

import pages.AndroidLocalPage;
import util.ScreenUtils;

public class AndroidLocal  extends BaseClass {
	
	@Test
	public void AndroidTest() throws InterruptedException {
		try {
			ScreenUtils.waitForVisibility(AndroidLocalPage.clearBtn());
			AndroidLocalPage.clearBtn().click();
			AndroidLocalPage.textInput().sendKeys(Search);
			AndroidLocalPage.translateBtn().click();
		}
		catch(Exception e)
		{
			System.out.println(e);
		}

	}

}
