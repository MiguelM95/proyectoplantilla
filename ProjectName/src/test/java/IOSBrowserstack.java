

import org.testng.Assert;
import org.testng.annotations.Test;

import pages.IOSBrowserstackPage;
import util.ScreenUtils;


public class IOSBrowserstack extends BaseClass{
	
	
	
	@Test
	public void IOSTest() throws InterruptedException {
		try {
			ScreenUtils.waitForVisibility(IOSBrowserstackPage.txtBtn());
			Thread.sleep(3000);
			IOSBrowserstackPage.txtBtn().click();
			IOSBrowserstackPage.txtInputBtn().sendKeys("hello@browserstack.com"+"\n");
			ScreenUtils.waitForVisibility(IOSBrowserstackPage.txtOutputBtn());
			Assert.assertEquals(IOSBrowserstackPage.txtOutputBtn().getText(),"hello@browserstack.com");
			}
		catch(Exception e)
		{
			System.out.println(e);
		}

	}

}
