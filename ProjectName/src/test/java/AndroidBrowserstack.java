
import org.testng.annotations.Test;

import pages.AndroidBrowserstackPage;
import util.ScreenUtils;

public class AndroidBrowserstack extends BaseClass{
	
	@Test
	public void AndroidTest() throws InterruptedException {
		try {
			  
			ScreenUtils.waitForVisibility(AndroidBrowserstackPage.titleLbl());
			AndroidBrowserstackPage.titleLbl().click();
			AndroidBrowserstackPage.searchTxt().sendKeys("BrowserStack");
		
		}
		catch(Exception e)
		{
			System.out.println(e);
		}

	}

}
