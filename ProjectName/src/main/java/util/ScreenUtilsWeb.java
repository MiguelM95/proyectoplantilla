package util;

import static driver.DriverManager.DRIVER;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public interface ScreenUtilsWeb {
	public static final long WAIT = 60;
	
	static void waitForVisibility(WebElement webElement) {
		WebDriverWait wait = new WebDriverWait(DRIVER.getDriver2(), ScreenUtils.WAIT);
		wait.until(ExpectedConditions.visibilityOf(webElement));
	}
}
