package pages;

import static com.codeborne.selenide.Selenide.$;

import org.openqa.selenium.WebElement;

import io.appium.java_client.MobileBy;



public class IOSBrowserstackPage {

	public static WebElement txtBtn() {
		return $(MobileBy.id("Text Button"));
	}
	
	public static WebElement txtInputBtn() {
		return $(MobileBy.id("Text Input"));
	}
	
	public static WebElement txtOutputBtn() {
		return  $(MobileBy.id("Text Output"));
	}
}
