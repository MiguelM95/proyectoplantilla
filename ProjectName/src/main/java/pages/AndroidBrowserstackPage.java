package pages;

import static com.codeborne.selenide.Selenide.$;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

public class AndroidBrowserstackPage
{
	public static WebElement titleLbl() {
		return $(By.xpath("//android.widget.TextView[@index=1]"));
	}
	
	public static WebElement searchTxt() {
		return $(By.id("org.wikipedia.alpha:id/search_src_text"));
	}
	
	
}
