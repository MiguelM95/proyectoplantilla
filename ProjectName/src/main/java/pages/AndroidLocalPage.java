package pages;

import static com.codeborne.selenide.Selenide.$;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

public class AndroidLocalPage {

	public static WebElement clearBtn() {
		return $(By.id("com.sqzsoft.texttospeech:id/buttonClear"));
	}

	public static WebElement textInput() {
		return $(By.id("com.sqzsoft.texttospeech:id/editTextInput"));
	}

	public static WebElement translateBtn() {
		return $(By.id("com.sqzsoft.texttospeech:id/buttonPlay"));
	}
	
}
