package driver;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;

import configuration.ProjectProperties;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.ios.IOSDriver;


import static configuration.ProjectProperties.HUB;
import static configuration.ProjectProperties.USER_DIR;
import static driver.AppiumServer.APPIUM_SERVICE;
import static configuration.Config.CONFIG;
import static configuration.ProjectProperties.DEVICE_PLATFORM;
import static configuration.ProjectProperties.DEVICE_PLATFORM2;
import static configuration.ProjectProperties.DEVICE_PLATFORM3;
import static configuration.ProjectProperties.DEVICE_PLATFORM4;
import static configuration.ProjectProperties.DEVICE_PLATFORM5;
import static configuration.ProjectProperties.DEVICE_PLATFORM6;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.logging.Logger;

public enum DriverManager {
	
	DRIVER;
	
	public WebDriver driver;
	
	private final ThreadLocal<WebDriver> webDriver = new ThreadLocal<>();
	private final ThreadLocal<AppiumDriver<MobileElement>> mobileDriver = new ThreadLocal<>();
	private final String DESIRED_CAPABILITIES_PATH_DIR = USER_DIR.concat("/resources/desired_capabilities/");
	private static Logger LOGGER = Logger.getLogger("InfoLogging");
	
	private String getHubURL() {
	    return APPIUM_SERVICE.getUrl();
	}

	private DesiredCapabilities getLocalDesiredCapabilities(String fileName) {
		 LOGGER.info("file name: "+fileName);
		return CONFIG.computeDesiredCaps(fileName);
	}

	private void setAndroidDriver(String remoteAddress, DesiredCapabilities capabilities) {
	    try {
	        mobileDriver.set(new AndroidDriver<>(new URL(remoteAddress), capabilities));
	    } catch (MalformedURLException e) {
	        e.printStackTrace();
	    }
	}

	private void setiOSDriver(String remoteAddress, DesiredCapabilities capabilities) {
	    try {
	    	 mobileDriver.set(new IOSDriver<>(new URL(remoteAddress), capabilities));
	    } catch (MalformedURLException e) {
	        e.printStackTrace();
	    }
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	private void setBrowserstackDriver(String remoteAddress, DesiredCapabilities capabilities) {
	    try {
	    	 mobileDriver.set(new AppiumDriver(new URL("http://hub.browserstack.com/wd/hub"), capabilities));
	    	 //<http://hub.browserstack.com/wd/hub>
	    } catch (MalformedURLException e) {
	        e.printStackTrace();
	    }
	}
	
	private void setWebBrowserstackDriver(DesiredCapabilities capabilities) {
	    try {
	    	 webDriver.set(new RemoteWebDriver(new URL("http://hub.browserstack.com/wd/hub"), capabilities));
	    } catch (MalformedURLException e) {
	        e.printStackTrace();
	    }
	}
	
	public boolean isFireFoxPlatform() {

	    return DEVICE_PLATFORM.contentEquals("firefox");
	}
	public boolean isChromePlatform() {

	    return DEVICE_PLATFORM2.contentEquals("chrome");
	}
	public boolean isSafariPlatform() {

	    return DEVICE_PLATFORM3.contentEquals("safari");
	}
	public boolean isAndroidPlatform() {

	    return DEVICE_PLATFORM4.contentEquals("android");
	}
	public boolean isIosPlatform() {

	    return DEVICE_PLATFORM5.contentEquals("ios");
	}
	

	private String getDesiredCapabilitiesPath() {
	    return DESIRED_CAPABILITIES_PATH_DIR
	            .concat(HUB)
	            .concat("_")
	            .concat(DEVICE_PLATFORM4)
	            .concat(".properties");
	}

	private String getDesiredCapabilitiesPath2() {
	    return DESIRED_CAPABILITIES_PATH_DIR
	            .concat(HUB)
	            .concat("_")
	            .concat(DEVICE_PLATFORM5)
	            .concat(".properties");

	}
	
	private String getDesiredCapabilitiesPath3() {
	    return DESIRED_CAPABILITIES_PATH_DIR
	            .concat(HUB)
	            .concat("_")
	            .concat(DEVICE_PLATFORM6)
	            .concat(".properties");

	}
	
	private String getDesiredCapabilitiesPath4() {
	    return DESIRED_CAPABILITIES_PATH_DIR
	            .concat(HUB)
	            .concat("_")
	            .concat(DEVICE_PLATFORM6)
	            .concat("_android")
	            .concat(".properties");

	}
	
	private String getDesiredCapabilitiesPath5() {
	    return DESIRED_CAPABILITIES_PATH_DIR
	            .concat(HUB)
	            .concat("_")
	            .concat(DEVICE_PLATFORM6)
	            .concat("_web")
	            .concat(".properties");

	}


	private DesiredCapabilities getDesiredCapabilities()
	{
	    return getLocalDesiredCapabilities(getDesiredCapabilitiesPath());
	}

	private DesiredCapabilities getDesiredCapabilities2()
	{
	    return getLocalDesiredCapabilities(getDesiredCapabilitiesPath2());
	}
	
	private DesiredCapabilities getDesiredCapabilities3()
	{
	    return getLocalDesiredCapabilities(getDesiredCapabilitiesPath3());
	}
	
	private DesiredCapabilities getDesiredCapabilities4()
	{
	    return getLocalDesiredCapabilities(getDesiredCapabilitiesPath4());
	}
	
	private DesiredCapabilities getDesiredCapabilities5()
	{
	    return getLocalDesiredCapabilities(getDesiredCapabilitiesPath5());
	}
	public void setDriver() {
	    setAndroidDriver(getHubURL(), getDesiredCapabilities());
	   // LOGGER.info("APP: ".concat((String) getDriver().getCapabilities().getCapability("app")));

	}

	public void setDriver2() {
		setiOSDriver(getHubURL(), getDesiredCapabilities2());
	  //  LOGGER.info("APP: ".concat((String) getDriver2().getCapabilities().getCapability("app")));

	}
	
	public void setDriver3() {
		setBrowserstackDriver(getHubURL(), getDesiredCapabilities3());
	  //  LOGGER.info("APP: ".concat((String) getDriver2().getCapabilities().getCapability("app")));

	}
	
	public void setDriver4() {
		setBrowserstackDriver(getHubURL(), getDesiredCapabilities4());
	  //  LOGGER.info("APP: ".concat((String) getDriver2().getCapabilities().getCapability("app")));

	}
	
	public void setDriver5() {
		setWebBrowserstackDriver(getDesiredCapabilities5());
	  //  LOGGER.info("APP: ".concat((String) getDriver2().getCapabilities().getCapability("app")));

	}
	
	 public WebDriver chromeDriverConnection() {
		 System.out.println(ProjectProperties.USER_DIR);
		 String configPath = ProjectProperties.USER_DIR
	                .concat("/resources/drivers/");
		 System.setProperty("webdriver.chrome.driver", configPath +"chromedriver.exe");
		 driver = new ChromeDriver();
		 webDriver.set(driver);
		 return driver;	 
	 }
	 

	 public WebDriver firefoxDriverConnection() {
		 String configPath = ProjectProperties.USER_DIR
	                .concat("/resources/drivers/");
		 System.setProperty("webdriver.gecko.driver", configPath +"geckodriver.exe");
		 driver = new FirefoxDriver();
		 webDriver.set(driver);
		return driver;	 
	 }
	 
	 public WebDriver edgeDriverConnection() {
		 String configPath = ProjectProperties.USER_DIR
	                .concat("/resources/drivers/");
		 System.setProperty("webdriver.edge.driver", configPath +"msedgedriver.exe");
		 driver = new EdgeDriver();
		 webDriver.set(driver);
		return driver;	 
	 }
	 

	 public void maximizeBrowser() {
		 driver.manage().window().maximize();
	 }
	 
	 
	 public AppiumDriver<MobileElement> getDriver() {
		    return mobileDriver.get();
		}
	 
	 public WebDriver getDriver2() {
			return webDriver.get();
	    }
	 
	 public WebDriver getURL(String url) {
		 driver.get(url);
		return driver;
	
	 }
	 
	public void closeDriver() {	
		 AppiumDriver<MobileElement> result;
		 result = getDriver();
		 
		 if(result != null)
		 {
			 getDriver().quit(); 
		 }
		 else
		 {
			 getDriver2().quit();
		 }  
	        
		 }
}
