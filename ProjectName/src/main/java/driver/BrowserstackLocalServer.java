package driver;

import java.util.HashMap;

import com.browserstack.local.Local;

public enum BrowserstackLocalServer
{
BROWSERSTACK_SERVICE;


	Local bsLocal = new Local();
	HashMap<String, String> bsLocalArgs = new HashMap<String, String>();
	{
		bsLocalArgs.put("key", "sqESkovD6aSqgdFmgEvi");
	}
	
	public void start() throws Exception {
		bsLocal.start(bsLocalArgs);	
	}
	
	public void stop() throws Exception {
		bsLocal.stop();
	}
}